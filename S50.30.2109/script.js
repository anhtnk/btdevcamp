"use strict";
$(document).ready(function() {

    // đối tượng chứa dữ liệu khi load trang 
    var gUserResObj = {
        users: [],
        // phương thức lọc dữ liệu
        filterUsers: function(paramFilterOrder) {
          //debugger;
          var vOrderResult = this.users.filter(function(paramOrder) {
              return (paramOrder.trangThai == paramFilterOrder.status || paramFilterOrder.status == "") 
                      && (paramOrder.loaiPizza !== null && paramOrder.loaiPizza.toLowerCase() == paramFilterOrder.loaiPizza || paramFilterOrder.loaiPizza == "")
          });
          return vOrderResult;
          }
        
    };

    //biến lưu id vào orderId của đơn order
    var gOrder = {
      id: "",
      orderId: ""
    }
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gCOLUMN_ORDER_ID = 0;
  const gCOLUMN_KICH_CO = 1;
  const gCOLUMN_LOAI_PIZZA = 2;
  const gCOLUMN_DRINK = 3;
  const gCOLUMN_THANH_TIEN = 4 ;
  const gCOLUMN_HO_TEN = 5;
  const gCOLUMN_SO_DIEN_THOAI = 6;
  const gCOLUMN_TRANG_THAI = 7;
  const gCOLUMN_ACTION = 8;
  
  // mảng hằng số chứa tên các thuộc tính
  var gORDER_COL = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];

  // định nghĩa table  - chưa có data
  var gUserTable = $("#order-table").DataTable( {
    // Khai báo các cột của datatable
    columns : [
        { "data" : gORDER_COL[gCOLUMN_ORDER_ID] },
        { "data" : gORDER_COL[gCOLUMN_KICH_CO] },
        { "data" : gORDER_COL[gCOLUMN_LOAI_PIZZA] },
        { "data" : gORDER_COL[gCOLUMN_DRINK] },
        { "data" : gORDER_COL[gCOLUMN_THANH_TIEN] },
        { "data" : gORDER_COL[gCOLUMN_HO_TEN] },
        { "data" : gORDER_COL[gCOLUMN_SO_DIEN_THOAI] },
        { "data" : gORDER_COL[gCOLUMN_TRANG_THAI] },
        { "data" : gORDER_COL[gCOLUMN_ACTION] }
    ],
    // Ghi đè nội dung của cột action, chuyển thành button chi tiết
    columnDefs: [ 
    {
        targets: gCOLUMN_ACTION,
        defaultContent: "<button class='info-user btn btn-info'>Chi tiết</button>"
    }]
  });
  
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  // gán click event handler cho button chi tiet
  $("#order-table").on("click", ".info-user", function() {
    onButtonChiTietClick(this); // this là button được ấn
  });

  // gán click event handler cho nút Lọc
  $("#btn-filter").on("click", onBtnFilterClick);

  // gán sự kiện cho btn Confirm 
  $("#btn-Confirm").on("click", onBtnConfirmClick);

  // gán sự kiện cho btn Cancel 
  $("#btn-Cancel").on("click", onBtnCancelClick);
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
  

  // hàm chạy khi trang được load
  function onPageLoading() {
    // gọi hàm load dữ liệu trạng thái  vào select
    loadDataToStatusSelect();
    // gọi hàm load dữ liệu loại pizza vào select
    loadDataToLoaiPizzaSelect();
    // lấy data từ server
    callDataFromServer();
  }

  // infoFunction sẽ là function các nút cùng gọi
  function onButtonChiTietClick(paramChiTietButton) {
    //1: thu thập id và order id của đơn order
    getIdOrderFromUser(paramChiTietButton);
    //2: call API lấy thông tim order
    callApiOrderByOrderId(gOrder.orderId);
    
    
  }

  // hàm thực hiện việc filter
  function onBtnFilterClick(){
      // Khai báo đối tượng chứa dữ liệu
      var vFilterData = {
        status: "",
        loaiPizza: ""
      };
      // B1: thu thập dữ liệu
      getFilterData(vFilterData);
      // B2: validate (ko cần)
      // B3: thực hiện filter
      var vUserResults = filterData(vFilterData);
      // B4: hiển thị dữ liệu lên front-end
      loadDataToTable(vUserResults);
  }

  // hàm thực hiện khi click confirm
  function onBtnConfirmClick(){
    "use strict";
    // khai báo đối tượng
    var vObjectConfirm = {
      trangThai: "confirmed"
    }
    console.log("Id : " + gOrder.id);
    //call API
    callApiConfirmOrder(vObjectConfirm, gOrder.id);
  }

  // hàm thực hiện khi click cancel
  function onBtnCancelClick(){
    // khai báo đối tượng
    var vObjectCanCelRequest = {
      trangThai: "cancel"
    }
    console.log(gOrder.id);
    //call API
    callApiCancelOrder(vObjectCanCelRequest, gOrder.id);
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
  
  // lấy data từ server 
  function callDataFromServer(){
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      type: "GET",
      dataType: 'json',
      success: function(responseObject){
        //console.log(responseObject);
        gUserResObj.users = responseObject;
        // load dữ liệu bảng
        loadDataToTable(gUserResObj.users);        
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });

  }
  // hàm thu thập dữ liệu
  function getFilterData(paramFilterDataObj) {
    paramFilterDataObj.status = $("#select-status").val();
    paramFilterDataObj.loaiPizza = $("#select-pizzaType").val();
  }

  // hàm thực hiện filter dữ liệu, trả về một mảng
  function filterData(paramFilterDataObj) {
    var vOrderResult = [];
      vOrderResult = gUserResObj.filterUsers(paramFilterDataObj)
      return vOrderResult;                             
 }

  // load data to table
  function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gUserTable.clear();
    //Cập nhật data cho bảng 
    gUserTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gUserTable.draw();
  }

  // load trang thai select
  function loadDataToStatusSelect() {
    var vStatusSelect = $("#select-status");
        vStatusSelect.append($("<option>", {text: "--- All ---", value: ""}))
        .append($("<option>", {text: "Open", value: "open"}))
        .append($("<option>", {text: "Confirmed", value: "confirmed"}))
        .append($("<option>", {text: "Cancel", value: "cancel"}));
  }
  // load Loại Pizza select
  function loadDataToLoaiPizzaSelect() {
    "use strict";
    var vPizzaTypeSelect = $("#select-pizzaType");
    vPizzaTypeSelect.append($("<option>", {text: "--- All ---", value: ""}))
        .append($("<option>", {text: "Hawaii", value: "hawaii"}))
        .append($("<option>", {text: "Bacon", value: "bacon"}))
        .append($("<option>", {text: "Seafood", value: "seafood"}));
  }

  function getIdOrderFromUser(paramOrderId){
    "use strict";
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramOrderId).parents('tr');
    //console.log(paramOrderId);
    //Lấy datatable row
    var vDatatableRow = gUserTable.row(vRowSelected); 
    //Lấy data của dòng 
    var vUserData = vDatatableRow.data();  
    var vId = vUserData.id;    
    var vOrderId = vUserData.orderId;
    var vKichCo = vUserData.kichCo;  
    var vLoaiPizza = vUserData.loaiPizza;     
    var vDrink = vUserData.idLoaiNuocUong;
    var vThanhTien = vUserData.thanhTien;
    var vHoTen = vUserData.hoTen;
    var vSoDienThoai = vUserData.soDienThoai;
    var vTrangThai = vUserData.trangThai;     
    //console.log("OrderId : " + vOrderId);
    //console.log("Kích Cỡ : " + vKichCo);
    //console.log("Loại Pizza : " + vLoaiPizza);
    //console.log("Nước uống : " + vDrink);
    //console.log("Thành tiền : " + vThanhTien);
    //console.log("Họ tên : " + vHoTen);
    //console.log("Số điện thoại : " + vSoDienThoai);
    //console.log("Trạng thái : " + vTrangThai);
    // gán id và orderid cho Obj 
    gOrder.id = vId;
    gOrder.orderId = vOrderId;
  }

  //hàm gọi API lấy data order
  function callApiOrderByOrderId(paramOrderId) {
    console.log(paramOrderId);
    $.ajax({
    url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramOrderId,
    type: "GET",
    dataType: 'json',
    success: function(responseObject){
      //console.log("Id : " + responseObject.id);
      //console.log("OrderId : " + responseObject.orderId);
      var vOrderRes = responseObject;
      //3: đổ data order lên form modal
      showDataOrderToFormModal(vOrderRes);
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
  }

  function showDataOrderToFormModal(paramResponseObject) {    
    $("#inp-id").val(paramResponseObject.id);
    $("#inp-orderId").val(paramResponseObject.orderId);
    $("#inp-fullname").val(paramResponseObject.hoTen);
    $("#inp-email").val(paramResponseObject.email);
    $("#inp-phonenumber").val(paramResponseObject.soDienThoai);
    $("#inp-address").val(paramResponseObject.diaChi);
    $("#inp-message").val(paramResponseObject.loiNhan);
    $("#inp-status").val(paramResponseObject.trangThai);
    $("#inp-ngaytao").val(paramResponseObject.ngayTao);
    $("#inp-ngaycapnhat").val(paramResponseObject.ngayCapNhat);
    $("#inp-combo-size").val(paramResponseObject.kichCo);
    $("#inp-pizza").val(paramResponseObject.loaiPizza);
    $("#inp-duongkinh").val(paramResponseObject.duongKinh);
    $("#inp-suonnuong").val(paramResponseObject.suon);
    $("#inp-drink").val(paramResponseObject.idLoaiNuocUong);
    $("#inp-soluongnuoc").val(paramResponseObject.soLuongNuoc);
    $("#inp-salad").val(paramResponseObject.salad);
    $("#inp-voucher").val(paramResponseObject.idVourcher);
    $("#inp-discount").val(paramResponseObject.giamGia);
    $("#inp-thanhtien").val(paramResponseObject.thanhTien);
    // show modal
    $("#modal-detail-order").modal("show");
  }

  // hàm goi api xác nhận confirm đơn order
  function callApiConfirmOrder(paramObjectRequest, paramId) {
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + paramId,
      type: "PUT",
      data: JSON.stringify(paramObjectRequest),
      contentType: "application/json",
      success: function(responseObject){
        //console.log(responseObject);
        //3: xử lý response
        handleConfirmOrder(responseObject);
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
  }

  //hàm xử lý response khi confirm order
  function handleConfirmOrder(paramResponseObject) {
    console.log(paramResponseObject);
    alert( "Confirm", "Order: " + paramResponseObject.orderId);
    callDataFromServer();
    //$("#modal-detail-order").modal("hide");
  }

  // hàm goi api xác nhận confirm đơn order
  function callApiCancelOrder(paramObjectRequest, paramId) {
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + paramId,
      type: "PUT",
      data: JSON.stringify(paramObjectRequest),
      contentType: "application/json",
      success: function(responseObject){
        //console.log(responseObject);
        //3: xử lý response
        handleCancelOrder(responseObject);
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
  }
  //hàm xử lý response khi cancel Order
  function handleCancelOrder(paramResponseObject) {
      console.log(paramResponseObject);
      callDataFromServer();
      alert( "Đã xoá", "Order: " + paramResponseObject.orderId);

  }

  


});